<?php
/**
 * Excerpt Arround Plugin
 *
 * @author Julien Gargot <julien@g-u-i.me>
 * @version 1.0.0
 */
function excerptArround($text, $search, $chars, $removehtml = true, $rep = '…') {


  $string = kirbytext($text);
  if($removehtml) $string = strip_tags($string);
  $string = str_replace(PHP_EOL, ' ', trim($string));
  if(strlen($string) <= $chars) return $string;

  // Looking for the the first word matching position.
  preg_match_all("/\w+/", $search, $words);
  $pos = false;
  $myword = false;
  foreach ($words[0] as $word)
  {
    if ( strlen($word) > 2 and $pos = strpos($string,$word) )
    {
      $myword = $word;
      break;
    }
  }
  $idealStartPos = $pos - ($chars / 2);
  $startpos = $idealStartPos > 0 && $idealStartPos < strlen($string) ? $idealStartPos : 0;

  return $chars == 0 ? $string : substr($string, $startpos, $chars) . $rep;

};
